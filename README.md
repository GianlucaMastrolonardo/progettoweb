# Da Implementare

- [x] A causa dell'ottimizzazione del DB si è spaccato un po' tutto, quindi bisogna fare un giro e vedere se tutto funziona.
      Le cose che ho trovato per ora sono problemi con Sign Up, mi aveva dato errore ma l'artista me l'ha comunque aggiunto.
      Ci sono problemi della visualizzaione di un artista quando un utente è loggato come user perché usa le vecchie query al db.
      In più c'è da controllare ancora creazione/cancellazione di un evento e acquisto di un biglietto.
- [x] Ottimizzare DB
- [x] Integrare nel sito Funzionalità acquisto biglietto (ora presente solo in `tmp.js`, ma funzionante).
- [x] Implementare Sign-Up.
- [x] Mandare E-Mail quando un utente acquista un biglietto con QR code per l'ingresso (nel QR ci salvi l'ID dell'evento + nome evento + username).
- [x] Aggiungere il nuovo DB con colonna per QR nel git (URGENTE!).
