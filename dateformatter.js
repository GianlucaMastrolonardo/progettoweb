"use strict";

function getDoubleDigitMinutes(minute) {
  const minuteParsed = parseInt(minute);
  return minuteParsed > 9 ? minuteParsed : `0${minuteParsed}`;
}

function formatDate(date) {
  if (!(date instanceof Date) || isNaN(date)) {
    throw new TypeError("Invalid Date");
  }
  return `${date
    .toDateString()
    .slice(4)}, ${date.getHours()}:${getDoubleDigitMinutes(date.getMinutes())}`;
}

module.exports = formatDate;
