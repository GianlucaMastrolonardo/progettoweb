/**
 * Spotify API, used for set artist picture if is not provided from DB
 */
"use strict";
const axios = require("axios");
const client_id = "d32a0290cbb54eabbd465f046e6237d3";
const client_secret = "7c0fa9729ccc4cb2be1819ff6f158ca7";

async function getToken() {
  const response = await axios.post(
    "https://accounts.spotify.com/api/token",
    new URLSearchParams({
      grant_type: "client_credentials",
    }),
    {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization:
          "Basic " +
          Buffer.from(client_id + ":" + client_secret).toString("base64"),
      },
      timeout: 30000,
    }
  );

  return response.data;
}

async function getArtistInfo(access_token, Artist_ID) {
  const response = await axios.get(
    `https://api.spotify.com/v1/artists/${Artist_ID}`,
    {
      headers: { Authorization: "Bearer " + access_token },
    }
  );

  return response.data;
}

function getArtistInfoFromSpotify(Artist_ID) {
  return getToken()
    .then((response) => {
      return getArtistInfo(response.access_token, Artist_ID);
    })
    .catch((error) => {
      console.error("Error fetching data:", error);
      throw error; // Rilancia l'errore per gestirlo a un livello superiore
    });
}

async function setArtistImageFromSpotify(artist) {
  try {
    const profile = await getArtistInfoFromSpotify(artist["Artist_SpotifyID"]);
    artist["Artist_IMG"] = profile.images[0].url;
  } catch (error) {
    console.error("Error setting artist image:", error);
    //Use fallback image
    artist["Artist_IMG"] = "http://localhost:3000/img/fallback";
  }

  return artist;
}

module.exports = { getArtistInfoFromSpotify, setArtistImageFromSpotify };
