/**
 * Connection and Query the database
 * Return only the data from the query, and not the structure, so result[0].
 */

"use strict";

const res = require("express/lib/response");
const mysql = require("mysql2/promise");
const { use } = require("passport");
const searchType = new Set([
  `Artists`,
  `Artist`,
  `ArtistFromID`,
  `Events`,
  `EventsBuyedByUser`,
  `EventsBuyedByUserOfThatArtist`,
  `Event`,
  `User`,
  `PurchaseTicket`,
  `InsertEvent`,
  `DeleteEvent`,
  `SelectArtistIDFromEventID`,
  `CheckIfUserHaveTicket`,
  `AddUser`,
  `AddArtist`,
  `SelectByEmail`,
  `UserGoogle`,
  `AddUserGoogle`,
  `EventsBuyedByUserGoogle`,
  `EventsBuyedByUserGoogleOfThatArtist`,
  `CheckIfUserGoogleHaveTicket`,
  `PurchaseTicketGoogle`,
]);
const QRCode = require("qrcode");

async function generateQR(text) {
  try {
    return await QRCode.toDataURL(text);
  } catch (err) {
    console.error(`Can't Generate QR Code: ${err}`);
  }
}

function throwDefaultError(...params) {
  if (params.length === 0) {
    throw new Error("Fetch Data DB ERROR: Invalid Parameter");
  } else {
    throw new Error(`${params.toString()} are required!`);
  }
}

const searchTypeMap = {
  /**
   * Retrieves user information by username.
   *
   * @param {mysql.Connection} connection - Database connection.
   * @param {string} [Username=null] - Username of the user to retrieve.
   * @returns {Promise<Object>} A promise that resolves to the user information, including tickets.
   * @throws {Error} If `Username` is not provided or if multiple users are found with the same username.
   */
  User: async (connection, Username = null) => {
    if (!Username) {
      throwDefaultError("Username");
    }
    let [response] = await connection.execute(
      `SELECT * FROM User WHERE User.User_Username = ?`,
      [Username]
    );

    if (response.length > 1) {
      throw new Error(
        "Found more than one User, but Username should be UNIQUE. The DB is Broken?"
      );
    }

    if (response.length === 0) {
      return {};
    }

    const userInfo = response[0];

    //Get User Tickets
    [userInfo.User_Tickets] = await connection.execute(
      `SELECT DISTINCT * FROM PartecipantsEvents JOIN CurrentEvent ON PartecipantsEvents.Event_ID = CurrentEvent.Event_ID JOIN Artist ON CurrentEvent.Artist_ID = Artist.Artist_ID WHERE PartecipantsEvents.User_ID = ? ORDER BY CurrentEvent.Event_Timestamp`,
      [userInfo.User_ID]
    );

    return userInfo;
  },

  /**
   * Retrieves user information by Google ID.
   *
   * @param {mysql.Connection} connection - Database connection.
   * @param {string} [UserID=null] - Google ID of the user to retrieve.
   * @returns {Promise<Object>} A promise that resolves to the user information, including tickets.
   * @throws {Error} If `UserID` is not provided or if multiple users are found with the same Google ID.
   */
  UserGoogle: async (connection, UserID = null) => {
    if (!UserID) {
      throwDefaultError("UserID");
    }
    let [response] = await connection.execute(
      `SELECT * FROM UserGoogle WHERE UserGoogle.UserGoogle_ID = ?`,
      [UserID]
    );

    if (response.length > 1) {
      throw new Error(
        "Found more than one User, but UserID should be UNIQUE. The DB is Broken?"
      );
    }

    if (response.length === 0) {
      return {};
    }

    const userInfo = response[0];

    //Get User Tickets
    [userInfo.User_Tickets] = await connection.execute(
      `SELECT DISTINCT * FROM PartecipantsEventsGoogle JOIN CurrentEvent ON PartecipantsEventsGoogle.Event_ID = CurrentEvent.Event_ID JOIN Artist ON CurrentEvent.Artist_ID = Artist.Artist_ID WHERE PartecipantsEventsGoogle.User_ID = ? ORDER BY CurrentEvent.Event_Timestamp`,
      [userInfo.UserGoogle_ID]
    );

    return userInfo;
  },

  /**
   * Retrieves artist information by artist name.
   *
   * @param {mysql.Connection} connection - Database connection.
   * @param {string} [ArtistName=null] - Name of the artist to retrieve.
   * @returns {Promise<Object>} A promise that resolves to the artist information, including events.
   * @throws {Error} If `ArtistName` is not provided or if multiple artists are found with the same name.
   */
  Artist: async (connection, ArtistName = null) => {
    if (!ArtistName) {
      throwDefaultError("ArtistName");
    }
    let [response] = await connection.execute(
      `SELECT * FROM Artist WHERE Artist.Artist_Name = ?`,
      [ArtistName]
    );

    if (response > 1) {
      throw new Error(
        "Found more than one Artist, but ArtistName should be UNIQUE. The DB is Broken?"
      );
    }

    if (response.length === 0) {
      return {};
    }

    const artistInfo = response[0];

    //Get Artist Events
    [artistInfo.Artist_Events] = await connection.execute(
      `SELECT * FROM CurrentEvent JOIN Artist ON CurrentEvent.Artist_ID = Artist.Artist_ID WHERE Artist.Artist_ID = ? ORDER BY CurrentEvent.Event_Timestamp`,
      [artistInfo.Artist_ID]
    );

    return artistInfo;
  },

  /**
   * Retrieves artist information by artist ID.
   *
   * @param {mysql.Connection} connection - Database connection.
   * @param {number} [ArtistID=null] - ID of the artist to retrieve.
   * @returns {Promise<Object>} A promise that resolves to the artist information, including events.
   * @throws {Error} If `ArtistID` is not provided or if multiple artists are found with the same ID.
   */
  ArtistFromID: async (connection, ArtistID = null) => {
    if (!ArtistID) {
      throwDefaultError("ArtistID");
    }

    let [response] = await connection.execute(
      `SELECT * FROM Artist WHERE Artist.Artist_ID = ?`,
      [ArtistID]
    );

    if (response > 1) {
      throw new Error(
        "Found more than one Artist, but ArtistID should be UNIQUE. The DB is Broken?"
      );
    }

    if (response.length === 0) {
      return {};
    }

    const artistInfo = response[0];

    //Get Artist Events
    [artistInfo.Artist_Events] = await connection.execute(
      `SELECT * FROM CurrentEvent JOIN Artist ON CurrentEvent.Artist_ID = Artist.Artist_ID WHERE Artist.Artist_ID = ? ORDER BY CurrentEvent.Event_Timestamp`,
      [artistInfo.Artist_ID]
    );

    return artistInfo;
  },

  /**
   * Retrieves a list of artists, optionally filtering by name.
   *
   * @param {mysql.Connection} connection - Database connection.
   * @param {string} [ArtistName=null] - Optional name to filter artists by.
   * @returns {Promise<Array>} A promise that resolves to an array of artist information, including events.
   */
  Artists: async (connection, ArtistName = null) => {
    let artists = [];
    let response;

    if (ArtistName) {
      [response] = await connection.execute(
        `SELECT * FROM Artist WHERE Artist.Artist_Name LIKE ? ORDER BY Artist.Artist_Name`,
        [`%${ArtistName}%`]
      );
    } else {
      [response] = await connection.execute(
        `SELECT * FROM Artist ORDER BY Artist.Artist_Name`
      );
    }
    artists = response;

    if (artists.length > 0) {
      for (let artist of artists) {
        [artist.Artist_Events] = await connection.execute(
          `SELECT * FROM CurrentEvent JOIN Artist ON CurrentEvent.Artist_ID = Artist.Artist_ID WHERE Artist.Artist_ID = ? ORDER BY CurrentEvent.Event_Timestamp`,
          [artist.Artist_ID]
        );
      }
    }
    return artists;
  },

  /**
   * Retrieves event information by event ID.
   *
   * @param {mysql.Connection} connection - Database connection.
   * @param {number} [Event_ID=null] - ID of the event to retrieve.
   * @returns {Promise<Array>} A promise that resolves to an array of event information.
   * @throws {Error} If `Event_ID` is not provided.
   */
  Event: async (connection, Event_ID = null) => {
    if (!Event_ID) {
      throwDefaultError("EventID");
    }
    const [response] = await connection.execute(
      `SELECT * FROM CurrentEvent WHERE Event_ID = ?`,
      [Event_ID]
    );

    return response;
  },

  /**
   * Purchases a ticket for a specific user for a specific event.
   *
   * @param {mysql.connection} connection - Database connection.
   * @param {number} UserID - ID of the user.
   * @param {number} EventID - ID of the event.
   * @param {string} UserType - The type of User, can be only `user` or `google-user`
   * @returns {Promise<Array>} Result of the SQL query.
   * @throws {Error} If `UserID` or `EventID` are not provided.
   */
  PurchaseTicket: async (
    connection,
    UserID = null,
    EventID = null,
    UserType = null
  ) => {
    if (!UserID || !EventID || !UserType) {
      throwDefaultError("UserID", "EventID", "UserType");
    }

    if (!(UserType === "user" || UserType === "google-user")) {
      throw new Error("Invalid UserType");
    }

    const [infoQRCode] = await connection.execute(
      `SELECT ${
        UserType === "user"
          ? "User.User_Username"
          : "UserGoogle.UserGoogle_DisplayName"
      }, ${
        UserType === "user" ? "User.User_Email" : "UserGoogle.UserGoogle_Email"
      }, CurrentEvent.Event_Name, CurrentEvent.Event_ID, Artist.Artist_Name FROM ${
        UserType === "user" ? "User" : "UserGoogle"
      }, CurrentEvent JOIN Artist ON CurrentEvent.Artist_ID = Artist.Artist_ID WHERE ${
        UserType === "user" ? "User.User_ID" : "UserGoogle.UserGoogle_ID"
      } = ? AND CurrentEvent.Event_ID = ?`,
      [UserID, EventID]
    );

    const QRCodeData = await generateQR(JSON.stringify(infoQRCode));

    //Add new ticket in the DB
    let [response] = await connection.execute(
      `INSERT INTO ${
        UserType === "user" ? "PartecipantsEvents" : "PartecipantsEventsGoogle"
      } (User_ID, Event_ID, Ticket_QRCode) VALUES (?, ?, ?)`,
      [UserID, EventID, QRCodeData]
    );

    if (response.affectedRows === 0) {
      throw new Error("Ticket not added correctly");
    }

    [response] = await connection.execute(
      `SELECT * FROM ${
        UserType === "user" ? "PartecipantsEvents" : "PartecipantsEventsGoogle"
      } WHERE User_ID = ? AND Event_ID = ?`,
      [UserID, EventID]
    );
    return response;
  },

  /**
   * Inserts a new event into the database.
   *
   * @param {mysql.connection} connection - Database connection.
   * @param {number} ArtistID - ID of the artist hosting the event.
   * @param {string} EventName - Name of the event.
   * @param {string} EventTimestamp - Timestamp of the event.
   * @param {string} EventCity - City where the event will take place.
   * @returns {Promise<Array>} Result of the SQL query.
   * @throws {Error} If any of the required parameters are not provided.
   */
  InsertEvent: async (
    connection,
    ArtistID = null,
    EventName = null,
    EventTimestamp = null,
    EventCity = null
  ) => {
    if (!ArtistID || !EventName || !EventTimestamp || !EventCity) {
      throwDefaultError("ArtistID", "EventName", "EventTimestamp", "EventCity");
    }

    const [response] = await connection.execute(
      `INSERT INTO CurrentEvent (Artist_ID, Event_Timestamp, Event_City, Event_Name) VALUES (?, ?, ?, ?)`,
      [ArtistID, EventTimestamp, EventCity, EventName]
    );

    return response;
  },

  /**
   * Deletes a specific event based on event ID and artist ID.
   *
   * @param {mysql.connection} connection - Database connection.
   * @param {number} EventID - ID of the event.
   * @param {number} ArtistID - ID of the artist hosting the event.
   * @returns {Promise<Array>} Result of the SQL query.
   * @throws {Error} If `EventID` or `ArtistID` are not provided.
   */
  DeleteEvent: async (connection, EventID = null, ArtistID = null) => {
    if (!EventID || !ArtistID) {
      throwDefaultError("EventID", "ArtistID");
    }

    const [response] = await connection.execute(
      `DELETE FROM CurrentEvent WHERE Event_ID = ? AND Artist_ID = ?`,
      [EventID, ArtistID]
    );
    return response;
  },

  /**
   * Adds a new user to the database.
   *
   * @param {mysql.connection} connection - Database connection.
   * @param {string} Username - Username of the new user.
   * @param {string} UserPasswordHash - Hashed password of the new user.
   * @param {string} UserEmail - Email of the new user.
   * @param {string} [UserImage=null] - Image of the new user (optional).
   * @returns {Promise<Array>} Result of the SQL query.
   * @throws {Error} If `Username`, `UserPasswordHash`, or `UserEmail` are not provided.
   */
  AddUser: async (
    connection,
    Username = null,
    UserPasswordHash = null,
    UserEmail = null,
    UserImage = null
  ) => {
    if (!Username || !UserPasswordHash || !UserEmail) {
      throwDefaultError("Username", "UserPasswordHash", "UserEmail");
    }
    const [response] = await connection.execute(
      `INSERT INTO User (
        User_Username,
        User_PasswordHash,
        User_Email,
        User_IMG
      )
      VALUES (?, ?, ?, ?);`,
      [Username, UserPasswordHash, UserEmail, UserImage]
    );
    return response;
  },

  /**
   * Adds a new artist to the database.
   *
   * @param {mysql.connection} connection - Database connection.
   * @param {string} ArtistName - Name of the new artist.
   * @param {string} ArtistPasswordHash - Hashed password of the new artist.
   * @param {string} ArtistEmail - Email of the new artist.
   * @param {string} [ArtistImage=null] - Image of the new artist (optional).
   * @param {string} [ArtistSpotifyID=null] - Spotify ID of the new artist (optional).
   * @returns {Promise<Array>} Result of the SQL query.
   * @throws {Error} If `ArtistName`, `ArtistPasswordHash`, or `ArtistEmail` are not provided.
   */
  AddArtist: async (
    connection,
    ArtistName = null,
    ArtistPasswordHash = null,
    ArtistEmail = null,
    ArtistImage = null,
    ArtistSpotifyID = null
  ) => {
    if (!ArtistName || !ArtistPasswordHash || !ArtistEmail) {
      throwDefaultError("ArtistName", "ArtistPasswordHash", "ArtistEmail");
    }
    const [response] = await connection.execute(
      `INSERT INTO Artist (
      Artist_Name,
      Artist_PasswordHash,
      Artist_Email,
      Artist_IMG,
      Artist_SpotifyID
      ) VALUES (?, ?, ?, ?, ?);`,
      [
        ArtistName,
        ArtistPasswordHash,
        ArtistEmail,
        ArtistImage,
        ArtistSpotifyID,
      ]
    );

    return response;
  },

  /**
   * Selects a user or artist by email.
   *
   * @param {mysql.connection} connection - Database connection.
   * @param {string} UserType - Type of user (either 'artist' or 'user').
   * @param {string} Email - Email to search for.
   * @returns {Promise<Array>} Result of the SQL query.
   * @throws {Error} If `UserType` is not 'artist' or 'user', or if `Email` is not provided.
   */
  SelectByEmail: async (connection, UserType = null, Email) => {
    if (!(UserType === "artist" || UserType === "user") || !Email) {
      throw new Error("UserType can be only Artist or User. Email is required");
    }
    UserType = UserType.charAt(0).toUpperCase() + UserType.slice(1);
    const [response] = await connection.execute(
      `SELECT * FROM ${UserType} WHERE ${UserType}_Email = ?`,
      [Email]
    );
    return response;
  },

  /**
   * Adds a new user to the database using Google authentication.
   *
   * @param {mysql.Connection} connection - Database connection.
   * @param {string} GoogleID - Google ID of the new user.
   * @param {string} GoogleDisplayName - Display name of the new user.
   * @param {string} GoogleEmail - Email of the new user.
   * @param {string} [GoogleIMG=null] - Image of the new user (optional).
   * @returns {Promise<Array>} Result of the SQL query.
   * @throws {Error} If `GoogleID`, `GoogleDisplayName`, or `GoogleEmail` are not provided.
   */
  AddUserGoogle: async (
    connection,
    GoogleID = null,
    GoogleDisplayName = null,
    GoogleEmail = null,
    GoogleIMG = null
  ) => {
    if (!GoogleID || !GoogleDisplayName || !GoogleEmail) {
      throwDefaultError("GoogleID", "GoogleDisplayName", "GoogleEmail");
    }
    const [response] = await connection.execute(
      `
    INSERT INTO UserGoogle (
    UserGoogle_ID,
    UserGoogle_DisplayName,
    UserGoogle_Email,
    UserGoogle_IMG)
    VALUES (?, ?, ?, ?);`,
      [GoogleID, GoogleDisplayName, GoogleEmail, GoogleIMG]
    );

    return response;
  },

  /**
   * Searches for events.
   * If `EventPlaceAndName` is provided, returns events matching the specified name or city.
   * If `EventPlaceAndName` is not provided, returns all events.
   *
   * @param {mysql.connection} connection - Database connection.
   * @param {string} [EventPlaceAndName=null] - Name or city of the event to search for (optional).
   * @returns {Promise<Array>} Result of the SQL query.
   */
  Events: async (connection, EventPlaceAndName = null) => {
    let response;
    if (EventPlaceAndName) {
      [response] = await connection.execute(
        `SELECT * FROM (
          SELECT * FROM CurrentEvent 
          WHERE CurrentEvent.Event_City LIKE ? 
          UNION 
          SELECT * FROM CurrentEvent 
          WHERE CurrentEvent.Event_Name LIKE ?
          ) AS CombinedResults JOIN Artist ON CombinedResults.Artist_ID = Artist.Artist_ID
          ORDER BY Event_Name`,
        [`%${EventPlaceAndName}%`, `%${EventPlaceAndName}%`]
      );
    } else {
      [response] = await connection.execute(
        `SELECT * FROM CurrentEvent JOIN Artist ON CurrentEvent.Artist_ID = Artist.Artist_ID ORDER BY CurrentEvent.Event_Name`
      );
    }
    return response;
  },
};

async function main(searchTypeInput, ...params) {
  //Only a specific set of searchType is valid, for avoid SQLInjection.
  if (!searchType.has(searchTypeInput)) {
    throw new TypeError(`Invalid Type of search: ${searchTypeInput}`);
  }
  const connection = await mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "Tonno1213!",
    database: "Shrimply",
  });
  try {
    const result = await searchTypeMap[searchTypeInput](connection, ...params);
    return result;
  } catch (err) {
    console.error(err);
  } finally {
    await connection.end();
  }
}

module.exports = main;
