"use strict";
const express = require("express");
const session = require("express-session");
const bcrypt = require("bcrypt");
const passport = require("passport");
const app = express();
const port = 3000;
const path = require("path");
const home = require("./routes/home");
const search = require("./routes/search");
const artist = require("./routes/artist");
const user = require("./routes/user");
const signup = require("./routes/signup");
const login = require("./routes/login");
const logout = require("./routes/logout");
const ticket = require("./routes/ticket");
const img = require("./routes/img");
const auth = require("./routes/auth");

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use(
  session({
    secret: bcrypt.hashSync("Shrimply", 10),
    resave: false,
    saveUninitialized: true,
  })
);

app.use(passport.authenticate("session"));

app.use("/", express.static(path.join(__dirname, "public")));
app.use("/", home);
app.use("/search", search);
app.use("/artist", artist);
app.use("/user", user);
app.use("/signup", signup);
app.use("/login", login);
app.use("/logout", logout);
app.use("/ticket", ticket);
app.use("/img", img);
app.use("/auth", auth);

app.listen(port, () => console.log("Listening"));
