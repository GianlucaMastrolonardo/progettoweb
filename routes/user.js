/**
 * User route:
 * Handles requests to retrieve user profile information. Checks user authentication based
 * on session data, retrieves the user profile and tickets from the database, formats the
 * event dates, and renders the user profile page. If the user is not authenticated or an
 * error occurs, returns appropriate HTTP status codes and error messages.
 */

"use strict";
const express = require("express");
const router = express.Router();
const db = require("../db");
const formatDate = require("../dateformatter");

function isAuthenticated(req) {
  if (
    req.session.usertype === "google-user" ||
    req.session.usertype === "user"
  ) {
    return req.session.user === req.params["username"];
  } else {
    return false;
  }
}

router.get("/@me", (req, res) => {
  if (
    req.session.user &&
    (req.session.usertype === `user` || req.session.usertype === `google-user`)
  ) {
    res.redirect(`http://localhost:3000/user/${req.session.user}`);
  } else {
    res.sendStatus(400);
  }
});

router.get("/:username", async (req, res) => {
  if (!isAuthenticated(req)) {
    res.sendStatus(401);
  } else {
    try {
      let userInfo;

      if (req.session.usertype === `user`) {
        userInfo = await db(`User`, req.session.user);
      } else if (req.session.usertype === `google-user`) {
        userInfo = await db(`UserGoogle`, req.session.userID);
        userInfo.User_Username = userInfo.UserGoogle_DisplayName;
      } else {
        res.status(400).send("Invalid User Type");
        return;
      }
      if (userInfo.length === 0) {
        res.status(500).send("Can't find user");
        return;
      }
      const eventsWithFormattedDate = userInfo.User_Tickets.map((event) => ({
        ...event,
        Event_Timestamp: formatDate(event.Event_Timestamp),
      }));
      if (userInfo.User_IMG) {
        userInfo.User_IMG = `http://localhost:3000/img/user/${userInfo.User_ID}`;
      }
      res.render("user", {
        user: userInfo,
        events: eventsWithFormattedDate,
        userType: req.session.usertype,
      });
    } catch (error) {
      res.status(500).send(`Error while loading user: ${error}`);
    }
  }
});

module.exports = router;
