/**
 * Search Route:
 * use DB for find artist, use SpotifyAPI for set Artist_IMG if is NULL.
 * If Artist Type Search (default search):
 *  -If no search query is provided display all artist, else display only the searched artists.
 * If Event Type Search: search city event
 * Send the output to the search view
 */

"use strict";
const express = require("express");
const router = express.Router();
const db = require("../db");
const { setArtistsImage } = require("../setArtistImage");
const formatDate = require("../dateformatter");

router.get("/", async (req, res) => {
  try {
    const reqType = Object.keys(req.query)[0];
    let resultArtist;
    let resultEvents;
    let groupedEventsByArtist;
    if (!reqType || reqType === `artist`) {
      //Artist Query
      const artist = req.query.artist;
      const queryType = "Artists";
      resultArtist = await db(queryType, artist);
      await setArtistsImage(resultArtist);
    } else if (reqType === `event`) {
      //Event Query
      const event = req.query.event;
      const queryType = "Events";
      resultEvents = await db(queryType, event);

      if (resultEvents) {
        //Format Date from epoch to human redable
        const eventsWithFormattedDate = resultEvents.map((event) => ({
          ...event,
          Event_Timestamp: formatDate(event.Event_Timestamp),
        }));
        //Group Events by Artist
        groupedEventsByArtist = Object.groupBy(
          eventsWithFormattedDate,
          ({ Artist_Name }) => Artist_Name
        );
      }
    } else {
      throw new TypeError("Invalid Search Type");
    }

    res.render("search", {
      artists: resultArtist,
      events: groupedEventsByArtist,
      userType: req.session.usertype,
    });
  } catch (error) {
    console.error("Errore durante la ricerca:", error);
    res.status(500).send(`Error while searching: ${error}`);
  }
});

module.exports = router;
