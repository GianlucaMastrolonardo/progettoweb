/**
 * Profile picture route handler:
 * Handles requests to retrieve user or artist profile pictures
 * based on the provided user type and ID. Ensures authentication
 * for users accessing their own profiles, retrieves image data
 * from the database, and serves the image or a fallback image
 * if necessary.
 */

"use strict";
const express = require("express");
const router = express.Router();
const path = require("path");
const db = require("../db");
const { constants } = require("vm");

function isAuthenticated(req) {
  return (
    //Only himself can see his profile picture
    req.session.userID == req.params["userID"] &&
    (req.session.usertype === "user" || req.session.usertype === "google-user")
  );
}

router.get("/:userType/:userID", async (req, res) => {
  if (
    !(req.params["userType"] === "user" || req.params["userType"] === "artist")
  ) {
    res.sendStatus(400);
    return;
  }
  if (!isAuthenticated(req) && req.params["userType"] === "user") {
    res.sendStatus(401);
    return;
  }

  let queryType;
  let queryParam;

  if (req.params["userType"] === "user") {
    if (!isAuthenticated(req)) {
      res.sendStatus(401);
      return;
    }
    queryType = req.session.usertype === "user" ? "User" : "UserGoogle";
    queryParam =
      req.session.usertype === "user" ? req.session.user : req.session.userID;
  } else if (req.params["userType"] === "artist") {
    queryType = "ArtistFromID";
    queryParam = req.params["userID"];
  } else {
    res.sendStatus(400);
    return;
  }

  let img;
  try {
    const user = await db(queryType, queryParam);
    if (queryType === "User") {
      img = user.User_IMG;
    } else if (queryType === "UserGoogle") {
      img = user.UserGoogle_IMG;
    } else {
      img = user.Artist_IMG;
    }
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
    return;
  }

  if (img) {
    try {
      res.setHeader("Content-Type", "image/png");
      res.send(img);
      return;
    } catch (err) {
      console.error(err);
      res.redirect("http://localhost:3000/img/fallback");
      return;
    }
  } else {
    res.redirect("http://localhost:3000/img/fallback");
    return;
  }
});

router.get("/fallback", (req, res) => {
  const filePath = path.join(__dirname, "../public/images/artist.jpg");
  try {
    res.sendFile(filePath);
  } catch (error) {
    res.sendStatus(500);
  }
});

module.exports = router;
