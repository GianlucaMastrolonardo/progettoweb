/**
 * Ticket purchase route:
 * Handles ticket purchase requests for users. Validates user authentication, checks if
 * the event exists, and ensures the user has not already purchased a ticket. If all checks
 * pass, records the ticket purchase in the database and sends a confirmation email with a
 * QR code as an attachment. Artists are restricted from purchasing tickets, and appropriate
 * error messages are sent for various failure scenarios.
 */

"use strict";
const express = require("express");
const router = express.Router();
const db = require("../db");
const Recipient = require("mailersend").Recipient;
const EmailParams = require("mailersend").EmailParams;
const MailerSend = require("mailersend").MailerSend;
const Sender = require("mailersend").Sender;
const Attachment = require("mailersend").Attachment;

const mailerSend = new MailerSend({
  apiKey:
    "mlsn.832194ee8ac07e7721bdf5186db0b3cc0471a7aae2c77cccf43117daddafd525",
});

function isAuthenticated(req) {
  return (
    req.session.usertype === `user` || req.session.usertype === `google-user`
  );
}

function hasTicket(userEvents, eventID) {
  return userEvents.some((event) => event.Event_ID === eventID);
}

router.put("/buy/:eventId", async (req, res) => {
  if (!isAuthenticated(req)) {
    if (req.session.usertype === `artist`) {
      res.status(401).send("Artists can't buy tickets");
    } else {
      res.sendStatus(401);
    }
    return;
  }
  const eventId = parseInt(req.params.eventId);
  if (!eventId) {
    res.status(400).send("Invalid Event ID");
    return;
  }
  //Check if event exist in DB, if not send 404
  let response = await db("Event", eventId);
  const eventInfo = response;
  if (response.length !== 1) {
    res.sendStatus(404);
    return;
  }

  //Check if user have already the ticket
  response = null;
  if (req.session.usertype === "user") {
    response = await db("User", req.session.user);
  } else {
    response = await db("UserGoogle", req.session.userID);
  }

  if (!response) {
    res.status(404).send("User not found");
  }
  const userInfo = response;

  if (hasTicket(response.User_Tickets, eventId)) {
    res.status(409).send("User already bougth this ticket");
    return;
  }

  //Buy ticket
  let sendEmail = false;
  response = null;
  try {
    response = await db(
      "PurchaseTicket",
      req.session.userID,
      eventId,
      req.session.usertype
    );

    if (!response || !response[0]) {
      throw new Error("Error when trying to buy a ticket");
    }
    res.sendStatus(200);
    sendEmail = true;
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }

  if (sendEmail) {
    let ticketInfo = response[0];
    if (req.session.usertype === "google-user") {
      userInfo.User_Email = userInfo.UserGoogle_Email;
      userInfo.User_Username = userInfo.UserGoogle_DisplayName;
    }

    const recipients = [
      new Recipient(userInfo.User_Email, userInfo.User_Username),
    ];

    const mailSender = new Sender(
      "ticket@trial-neqvygmx578l0p7w.mlsender.net",
      "Shrimply"
    );

    // Create the attachment
    const QRCodeEmail = [
      new Attachment(ticketInfo.Ticket_QRCode.split(",")[1], "Ticket.png"),
    ];

    const emailParams = new EmailParams()
      .setSubject(`Thanks for your purchase ${userInfo.User_Username}`)
      .setFrom(mailSender)
      .setTo(recipients)
      .setReplyTo(mailSender)
      .setHtml(
        `<h1>Purchase Completed!</h1>
        <br />
        <h3>${eventInfo[0].Event_Name} | ${eventInfo[0].Event_City}</h3>
        <br />
        <p>Hi <strong>${userInfo.User_Username}</strong>, thanks for your purchase on Shrimply</p>`
      )
      .setAttachments(QRCodeEmail);

    await mailerSend.email.send(emailParams).catch((response) => {
      console.log(response);
    });
  }
});

module.exports = router;
