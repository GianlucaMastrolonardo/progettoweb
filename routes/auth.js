/**
 * Google OAuth2 authentication route handler:
 * Configures Google OAuth2 strategy for user authentication using Passport.js.
 * Handles Google login, callback, and user session management. Checks if a
 * user authenticated through Google is already stored in the database, stores
 * new users if necessary, and manages user sessions for subsequent requests.
 */

"use strict";
const express = require("express");
const router = express.Router();
const passport = require("passport");
const GoogleStrategy = require("passport-google-oidc");
const db = require("../db");

passport.use(
  new GoogleStrategy(
    {
      clientID:
        "58779215489-6p2j2f7225cibudmtodsnv7oq3lbnasr.apps.googleusercontent.com",
      clientSecret: "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",
      callbackURL: "/auth/oauth2/redirect/google",
    },
    function (issuer, profile, cb) {
      return cb(null, profile);
    }
  )
);

passport.serializeUser((user, done) => {
  done(null, user); // user it's saved in the session
});

passport.deserializeUser((user, done) => {
  done(null, user); // user it's get from the session
});

router.get(
  "/google",
  passport.authenticate("google", { scope: ["profile", "email"] })
);

router.get(
  "/oauth2/redirect/google",
  passport.authenticate("google", {
    failureRedirect:
      "http://localhost:3000/login?error=Error while loading user information",
  }),
  async function (req, res) {
    //Check if Google ID is already stored in the DB
    const userInfo = await db("UserGoogle", req.session.passport.user.id);
    if (userInfo.length === 0) {
      //New user, add the user in the DB
      const response = await db(
        "AddUserGoogle",
        req.session.passport.user.id,
        req.session.passport.user.displayName,
        req.session.passport.user.emails[0].value
      );
    }
    //Set session
    req.session.user = req.session.passport.user.displayName;
    req.session.usertype = "google-user";
    req.session.userID = req.session.passport.user.id;
    req.session.save();
    res.redirect("http://localhost:3000/user/@me");
  }
);

router.get("/profile", (req, res) => {
  if (!req.isAuthenticated()) {
    return res.redirect("/");
  }
  res.json({ user: req.user });
});

module.exports = router;
