/**
 * Signup route handler:
 * Handles user and artist registration requests. Validates input fields, checks for
 * existing usernames and emails in the database, and verifies uploaded images for
 * correct type and size. If the registration is for an artist, also checks if the
 * provided Spotify ID is valid. If all checks pass, it hashes the user's password,
 * stores the new user or artist in the database, and sets up the session for the
 * newly created user or artist, redirecting to their respective profile page.
 */

"use strict";
const express = require("express");
const router = express.Router();
const multer = require("multer");
const upload = multer({ storage: multer.memoryStorage() });
const bcrypt = require("bcrypt");
const db = require("../db");
const { getArtistInfoFromSpotify } = require("../spotify");

function isAuthenticated(req) {
  return req.session.user == undefined ? false : true;
}

router.get("/", (req, res) => {
  if (isAuthenticated(req)) {
    //Can't signup if authenticated
    res.redirect(
      `http://localhost:3000/${req.session.usertype}/${req.session.user}`
    );
    return;
  }
  res.render("signup", { userType: undefined });
});

router.post(
  "/",
  upload.single("picture"),
  express.urlencoded({ extended: false }),

  async (req, res) => {
    if (!(req.body.username && req.body.email && req.body.password)) {
      res.status(400).send("Missing a field of the form!");
      return;
    }

    if (req.body.password.length < 5) {
      res.status(400).send("Password is too short!");
      return;
    }

    //Check if parameters are corrects
    if (
      !req.body.logintype ||
      !(req.body.logintype === "artist" || req.body.logintype === "user")
    ) {
      res.status(400).send("Invalid Login Type");
      return;
    }

    //Check if user or artist is already sign up with same name or same email
    //Name check
    const capitalLoginType =
      req.body.logintype.charAt(0).toUpperCase() + req.body.logintype.slice(1);
    let response = await db(capitalLoginType, req.body.username);

    if (Object.values(response).length > 0) {
      res.status(400).send(`Name ${req.body.username} is Already used`);
      return;
    }

    //Email check
    response = await db("SelectByEmail", req.body.logintype, req.body.email);

    if (response.length > 0) {
      res.status(400).send(`Email ${req.body.email} is Already used`);
      return;
    }

    //Check if image size it's less then 5MB or it's not correct type
    if (req.file) {
      if (req.file.size >= 5000000) {
        res.status(400).send(`Profile Picture is too big!`);
        return;
      }
      if (
        req.file.mimetype !== "image/png" &&
        req.file.mimetype !== "image/jpeg"
      ) {
        res.status(400).send(`Invalid File Type`);
        return;
      }
    }

    //Check if spotify id of the artist is valid
    let spotifyID = null;
    if (req.body.spotify && req.body.logintype === "artist") {
      try {
        spotifyID = req.body.spotify.match(/(?<=artist\/)[^\/?]+/)[0];
        await getArtistInfoFromSpotify(spotifyID);
      } catch (error) {
        console.error(error);
        res.status(400).send(`Invalid Spotify ID`);
        return;
      }
    }

    try {
      //Hash Password
      const hashPassword = bcrypt.hashSync(req.body.password, 10);

      if (req.body.logintype === `user`) {
        response = await db(
          `AddUser`,
          req.body.username,
          hashPassword,
          req.body.email,
          req.file ? req.file.buffer : null
        );
      } else {
        response = await db(
          `AddArtist`,
          req.body.username,
          hashPassword,
          req.body.email,
          req.file ? req.file.buffer : null,
          spotifyID
        );
      }
    } catch (err) {
      res.status(500).send("Something went wrong.");
      console.error(err);
      return;
    }
    const affectedRows = response.affectedRows;
    if (response && affectedRows === 1) {
      //Get the ID of the user/artist
      const userInfo = await db(
        req.body.logintype.charAt(0).toUpperCase() +
          req.body.logintype.slice(1),
        req.body.username
      );

      req.session.regenerate(async (err) => {
        req.session.user = req.body.username;
        req.session.usertype = req.body.logintype;
        req.session.userID =
          req.body.logintype === "user" ? userInfo.User_ID : userInfo.Artist_ID;
        req.session.save(function (err) {
          if (err) return next(err);
          const redirectURL = `http://localhost:3000/${req.session.usertype}/${req.session.user}`;
          res.redirect(redirectURL);
        });
      });
    } else {
      res.status(400).send("Something went wrong");
      return;
    }
  }
);

module.exports = router;
