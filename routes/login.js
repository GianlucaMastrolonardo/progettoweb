/**
 * Login route handler:
 * Handles GET and POST requests for user and artist login.
 * Renders the login page, validates login credentials,
 * checks the database for user/artist information, and
 * manages user sessions, ensuring secure authentication.
 */

"use strict";
const express = require("express");
const router = express.Router();
const db = require("../db");
const bcrypt = require("bcrypt");
const { use } = require("bcrypt/promises");

router.get("/", (req, res) => {
  try {
    res.render("login", { userType: undefined });
  } catch (error) {
    res.status(500).send(`Error while loading user: ${error}`);
  }
});

router.use(express.json()); // KEK Without this nothing work...

router.post("/", (req, res) => {
  // login logic to validate req.body.user and req.body.pass
  // would be implemented here. for this example any combo works

  // regenerate the session, which is good practice to help
  // guard against forms of session fixation
  req.session.regenerate(async (err) => {
    if (!req.body.logintype || !req.body.username || !req.body.password) {
      res.status(400).send("All Fields are Required!");
      return;
    }

    let userInfo;
    if (req.body.logintype === `user`) {
      userInfo = await db("User", req.body.username);
    } else if (req.body.logintype === `artist`) {
      userInfo = await db("Artist", req.body.username);
    } else {
      res.status(400).send("Invalid Login Type");
      return;
    }

    if (Object.values(userInfo).length === 0) {
      //User/Artist don't founded in the DB
      res
        .status(404)
        .send(`${req.body.logintype === "user" ? "User" : "Artist"} not found`);
      return;
    }

    const passwordHash =
      req.body.logintype === `user`
        ? userInfo.User_PasswordHash
        : userInfo.Artist_PasswordHash;

    if (!bcrypt.compareSync(req.body.password, passwordHash)) {
      res.status(401).send("Incorrect Name or Password");
      return;
    }
    if (err) next(err);

    // store user information in session, typically a user id
    req.session.user = req.body.username;
    req.session.usertype = req.body.logintype;
    req.session.userID =
      req.body.logintype === "user" ? userInfo.User_ID : userInfo.Artist_ID;
    // save the session before redirection to ensure page
    // load does not happen before session is saved
    req.session.save(function (err) {
      if (err) return next(err);
      res.sendStatus(200);
      return;
    });
  });
});

module.exports = router;
