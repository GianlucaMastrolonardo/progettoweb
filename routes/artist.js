/**
 * Artist route handler:
 * Manages artist-related routes, including displaying artist details,
 * events.
 * Uses the database to find and update artist information.
 * Utilizes the Spotify API to set the artist's image if not provided
 * in the database, ensures authentication, and formats event dates.
 */

"use strict";
const express = require("express");
const router = express.Router();
const db = require("../db");
const { setArtistImage } = require("../setArtistImage");
const formatDate = require("../dateformatter");

function isLoggedUser(req) {
  return req.session.usertype === `user` ||
    req.session.usertype === `google-user`
    ? true
    : false;
}

function isAuthenticated(req) {
  return req.session.user === req.params["artistName"];
}

async function getArtistInfoJson(req) {
  let queryType = "Artist";
  const artist = await db(queryType, req.params["artistName"]);
  if (Object.values(artist).length === 0) {
    return null;
  } else {
    return [artist];
  }
}

async function getArtistInfo(req, formatDateEvents) {
  let queryType = "Artist";
  const artist = await db(queryType, req.params["artistName"]);
  if (Object.values(artist).length === 0) {
    return null;
  } else {
    await setArtistImage(artist);
    if (formatDateEvents) {
      const eventsWithFormattedDate = artist.Artist_Events.map((event) => ({
        ...event,
        Event_Timestamp: formatDate(event.Event_Timestamp),
      }));
      return [artist, eventsWithFormattedDate];
    } else {
      return [artist];
    }
  }
}

async function isDateEventValid(req) {
  const ArtistInfo = await getArtistInfo(req, false);
  const dateNewEvent = new Date(req.body.eventDateParsed);

  const currentArtistEvents = ArtistInfo[0].Artist_Events;
  for (let event of currentArtistEvents) {
    if (
      event.Event_Timestamp.getDate() === dateNewEvent.getDate() &&
      event.Event_Timestamp.getMonth() === dateNewEvent.getMonth() &&
      event.Event_Timestamp.getFullYear() === dateNewEvent.getFullYear()
    ) {
      //Exist already an event of that artist in the same date. //Send 400
      return false;
    }
  }
  return true;
}

router.get("/@me", (req, res) => {
  if (req.session.usertype === `artist`) {
    res.redirect(`http://localhost:3000/artist/${req.session.user}`);
  } else {
    res.sendStatus(400);
  }
});

router.get("/:artistName", async (req, res) => {
  try {
    if (req.query.format === "json") {
      res.setHeader("Content-Type", "application/json");
      const result = await getArtistInfoJson(req);
      if (!result) {
        res.sendStatus(404);
        return;
      } else {
        res.json(result);
        return;
      }
    }
    const result = await getArtistInfo(req, true);
    if (!result) {
      res.status(404).send("Artist Not Found");
      return;
    } else {
      if (isLoggedUser(req)) {
        //Find all events buyed by that user of this specific artist
        let userEvents = [];

        if (req.session.usertype === "user") {
          const userInfo = await db("User", req.session.user);
          userEvents = userInfo.User_Tickets;
        } else {
          const userInfo = await db("UserGoogle", req.session.userID);
          userEvents = userInfo.User_Tickets;
        }

        if (userEvents.length > 0) {
          Object.keys(result[1]).forEach((key) => {
            result[1][key].alreadyBought = false;
            Object.keys(userEvents).forEach((keyUserEvents) => {
              if (
                result[1][key].Event_ID === userEvents[keyUserEvents].Event_ID
              ) {
                //Already bought ticket
                result[1][key].alreadyBought = true;
              }
            });
          });
        }
      }
      res.render("artist", {
        artist: result[0],
        events: result[1],
        isLogged: isAuthenticated(req),
        userType: req.session.usertype,
      });
    }
  } catch (error) {
    res.status(500).send(`Error while loading artist: ${error}`);
    console.error(error);
  }
});

router.get("/:artistName/newevent", async (req, res) => {
  if (!isAuthenticated(req)) {
    res.sendStatus(401);
    return;
  }
  const result = await getArtistInfo(req, false);
  res.render("newevent", { artist: result[0], userType: req.session.usertype });
});

router.use(express.json()); // KEK Without this nothing work...

router.put("/:artistName/newevent", async (req, res) => {
  if (!isAuthenticated(req)) {
    res.sendStatus(401);
    return;
  }
  const dateEvent = new Date(req.body.eventDateParsed);
  if ((await isDateEventValid(req)) && dateEvent > Date.now()) {
    try {
      const x = await db(
        "InsertEvent",
        req.session.userID,
        req.body.eventName,
        new Date(req.body.eventDateParsed),
        req.body.eventCity
      );
      if (x == undefined) {
        throw new Error("Unable to add new event");
      }
      res.sendStatus(200);
    } catch (err) {
      console.error(err);
      res.sendStatus(500);
    }
  } else {
    //Invalid Date
    res.sendStatus(400);
  }
});

router.delete("/:artistName/event/:eventId", async (req, res) => {
  if (!isAuthenticated(req)) {
    res.sendStatus(401);
    return;
  }

  const artistID = req.session.userID;
  const [eventInfo] = await db("Event", req.params.eventId);

  if (!eventInfo) {
    res.status(404).send("Can't find the event");
    return;
  }

  if (eventInfo.Artist_ID !== req.session.userID) {
    //User is authenticated but try to delete an event of another artist, so send 401
    res.status(401);
    return;
  }

  try {
    const response = await db("DeleteEvent", req.params.eventId, artistID);
    const statusCode = response.affectedRows === 1 ? 204 : 400;
    res.sendStatus(statusCode);
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});

module.exports = router;
