/**
 * Logout route handler:
 * Clears the user session, saves the session state,
 * and regenerates a new session to protect against
 * session fixation attacks before redirecting to the homepage.
 */

"use strict";
const express = require("express");
const router = express.Router();

router.get("/", function (req, res, next) {
  req.session.user = null;
  req.session.save(function (err) {
    if (err) next(err);

    // regenerate the session, which is good practice to help
    // guard against forms of session fixation
    req.session.regenerate(function (err) {
      if (err) next(err);
      res.redirect("http://localhost:3000");
    });
  });
});

module.exports = router;
