/**
 * Home route
 */

"use strict";
const express = require("express");
const router = express.Router();
const db = require("../db");
const bcrypt = require("bcrypt");
const { use } = require("bcrypt/promises");

router.get("/", (req, res) => {
  try {
    res.render("home", {
      user: req.session.user,
      userType: req.session.usertype,
    });
  } catch (error) {
    res.status(500).send(`Error while loading home: ${error}`);
  }
});

module.exports = router;
