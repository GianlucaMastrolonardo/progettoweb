"use strict";

function renderMessage(message, messageType) {
  let iconClass;
  if (messageType === "danger") {
    iconClass = "nf-cod-error";
  } else if (messageType === "success") {
    iconClass = "nf-cod-check";
  } else if (messageType === "warning") {
    iconClass = "nf-cod-warning";
  }
  return `
  <div class="container text-center" id="error-msg">
  <div class="alert alert-${messageType} ms-auto me-auto" id="login-error" role="alert">
  <i class ="nf ${iconClass}"></i>
         <strong>${message}</strong>
</div>
</div>`;
}

function printMessage(message, messageType) {
  //Remove previously alerts...
  if (document.getElementsByClassName("alert")) {
    document.querySelectorAll(".alert").forEach((el) => el.remove());
  }
  //Display new alert
  const top = document.getElementById("welcome-section");
  top.insertAdjacentHTML("afterend", renderMessage(message, messageType));
}

async function sendEventToBackEnd(eventName, eventDateParsed, eventCity) {
  try {
    const response = await fetch(window.location.href, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ eventName, eventDateParsed, eventCity }),
    });
    return response.status;
  } catch (err) {
    return 500;
  }
}

document.addEventListener("DOMContentLoaded", () => {
  const form = document.getElementById("createNewEvent");

  form.addEventListener("submit", async (event) => {
    const err = document.getElementById("error-msg");

    event.preventDefault(); // Prevent the form from submitting

    // Read the values of the form fields
    const eventName = document.getElementById("eventname").value;
    const eventDate = document.getElementById("eventdate").value;
    const eventCity = document.getElementById("eventcity").value;

    const eventDateParsed = new Date(eventDate);
    if (eventDateParsed <= Date.now()) {
      printMessage("Event Date is in the past", "warning"); //Date is invalid, print error
      return; //Don't send data to Backend
    } else if (err) {
      err.remove(); //Date now is valid, remove old alert error
    }

    const response = await sendEventToBackEnd(
      eventName,
      eventDateParsed,
      eventCity
    );
    if (response === 200) {
      printMessage("Event Added Successfully", "success");
      //Redirect to artist page
      const url = window.location.href;
      const redirect = url.replace("/newevent", "");
      setTimeout(() => {
        window.location.href = redirect;
      }, 5000);
    } else if (response === 400) {
      printMessage(
        "Invalid Date.<br /> Maybe another event have the same date",
        "warning"
      );
    } else {
      printMessage("Something Went Wrong.<br /> Please try later", "danger");
    }
  });
});
