"use strict";

function renderMessage(message, messageType) {
  let iconClass;
  if (messageType === "danger") {
    iconClass = "nf-cod-error";
  } else if (messageType === "success") {
    iconClass = "nf-cod-check";
  } else if (messageType === "warning") {
    iconClass = "nf-cod-warning";
  }
  return `
  <div class="container text-center" id="error-msg">
  <div class="alert alert-${messageType} ms-auto me-auto" id="login-error" role="alert">
  <i class ="nf ${iconClass}"></i>
         <strong>${message}</strong>
</div>
</div>`;
}

function printMessage(message, messageType) {
  //Remove previously alerts...
  if (document.getElementsByClassName("alert")) {
    document.querySelectorAll(".alert").forEach((el) => el.remove());
  }
  //Display new alert
  const top = document.getElementById("welcome-section");
  top.insertAdjacentHTML("afterend", renderMessage(message, messageType));
}

function renderGoogleButton() {
  return `
  <a href="http://localhost:3000/auth/google" >
  <button class="gsi-material-button" id="google-btn" type="button">
    <div class="gsi-material-button-state"></div>
    <div class="gsi-material-button-content-wrapper">
      <div class="gsi-material-button-icon">
        <svg
          version="1.1"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 48 48"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          style="display: block;"
        >
          <path
            fill="#EA4335"
            d="M24 9.5c3.54 0 6.71 1.22 9.21 3.6l6.85-6.85C35.9 2.38 30.47 0 24 0 14.62 0 6.51 5.38 2.56 13.22l7.98 6.19C12.43 13.72 17.74 9.5 24 9.5z"
          ></path>
          <path
            fill="#4285F4"
            d="M46.98 24.55c0-1.57-.15-3.09-.38-4.55H24v9.02h12.94c-.58 2.96-2.26 5.48-4.78 7.18l7.73 6c4.51-4.18 7.09-10.36 7.09-17.65z"
          ></path>
          <path
            fill="#FBBC05"
            d="M10.53 28.59c-.48-1.45-.76-2.99-.76-4.59s.27-3.14.76-4.59l-7.98-6.19C.92 16.46 0 20.12 0 24c0 3.88.92 7.54 2.56 10.78l7.97-6.19z"
          ></path>
          <path
            fill="#34A853"
            d="M24 48c6.48 0 11.93-2.13 15.89-5.81l-7.73-6c-2.15 1.45-4.92 2.3-8.16 2.3-6.26 0-11.57-4.22-13.47-9.91l-7.98 6.19C6.51 42.62 14.62 48 24 48z"
          ></path>
          <path fill="none" d="M0 0h48v48H0z"></path>
        </svg>
      </div>
      <span class="gsi-material-button-contents">Continue with Google</span>
      <span style="display: none;">Continue with Google</span>
    </div>
  </button>
  </a>
  `;
}

function renderSelector() {
  return `<label for="LoginTypeSelector" class="form-label"
          >Select user type</label
        >
        <select
          name="logintype"
          id="LoginTypeSelector"
          class="form-select mb-3"
          aria-label="select"
          aria-placeholder="Select user type"
          required
        >
          <option value=""></option>
          <option value="user">User</option>
          <option value="artist">Artist</option>
        </select>`;
}

function renderForm(userType) {
  return `
  <div id="form-fields">
  <div class="text-center">
  ${userType === "user" ? renderGoogleButton() : ""}
  </div>
  <hr />
  <div class="mb-3">
            <label for="InputUsername" class="form-label">
            ${userType === "artist" ? "Artist Name" : "Username"}
            </label>
            <input
              type="text"
              class="form-control"
              name="username"
              id="InputUsername"
              aria-describedby="emailHelp"
              required
            />
          </div>
          <div class="mb-3">
            <label for="InputPassword" class="form-label">Password</label>
            <div class="input-group mb-3">
              <input
                type="password"
                class="form-control"
                name="password"
                id="InputPassword"
                aria-describedby="passwordHelp"
                required
              />
              <button id="button-show-password" class="btn btn-outline-light" type="button">
                <i id="btn-eye" class="nf nf-fa-eye_slash"></i>
              </button>
            </div>
          </div>

          <button type="button" class="btn btn-shrimply-1" id="login-btn">Login</button>
          </div>`;
}

function setAttributePasswordLabel(passwordLabel) {
  if (passwordLabel.getAttribute(`type`) === `text`) {
    passwordLabel.setAttribute(`type`, `password`);
  } else {
    passwordLabel.setAttribute(`type`, `text`);
  }
}

function setEyeClass(button) {
  if (button.classList.contains(`nf-fa-eye_slash`)) {
    button.classList.remove(`nf-fa-eye_slash`);
    button.classList.add(`nf-fa-eye`);
  } else {
    button.classList.remove(`nf-fa-eye`);
    button.classList.add(`nf-fa-eye_slash`);
  }
}

async function sendInfoToBackEnd(username, password, logintype) {
  try {
    const response = await fetch(window.location.href, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ username, password, logintype }),
    });
    return [response.status, await response.text()];
  } catch (error) {
    console.error(error);
    return 500;
  }
}

function loadForm() {
  const divForm = document.getElementById(`login-form`);
  divForm.innerHTML = renderSelector();
  const loginType = document.getElementById("LoginTypeSelector");
  loginType.addEventListener(`change`, (event) => {
    const existingForm = document.getElementById("form-fields");
    if (existingForm) {
      existingForm.remove();
    }
    if (event.target.value !== ``) {
      const divField = document.createElement(`div`);
      divField.innerHTML = renderForm(event.target.value);
      while (divField.firstChild) {
        divForm.appendChild(divField.firstChild);
      }
      const buttonPassword = document.getElementById("button-show-password");
      const buttonPasswordStyle = document.getElementById("btn-eye");
      const passwordLabel = document.getElementById("InputPassword");
      buttonPassword.addEventListener(`click`, function () {
        setAttributePasswordLabel(passwordLabel);
        setEyeClass(buttonPasswordStyle);
      });

      const loginBtn = document.getElementById("login-btn");
      loginBtn.addEventListener("click", async (event) => {
        const username = document.getElementById("InputUsername").value;
        const password = document.getElementById("InputPassword").value;
        const logintype = document.getElementById("LoginTypeSelector").value;

        if (!username || !password) {
          loginBtn.setAttribute("type", "submit");
          return;
        } else {
          loginBtn.setAttribute("type", "button");
          event.preventDefault(); // Don't submit with standard POST method, but use fetch instead
        }
        const response = await sendInfoToBackEnd(username, password, logintype);
        if (response[0] === 200) {
          //Redirect to user/artist page
          const url = new URL(`http://localhost:3000/${logintype}/@me`);
          window.location.href = url;
          return;
        } else if (response[0] >= 400) {
          //Print error
          printMessage(response[1], "danger");
        }
      });
    }
  });
}

document.addEventListener(`DOMContentLoaded`, loadForm);
