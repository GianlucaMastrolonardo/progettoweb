"use strict";

document.addEventListener("DOMContentLoaded", () => {
  const searchBar = document.getElementById("search-label");
  const dropdownItems = document.querySelectorAll(".dropdown-item");
  const dropdownButton = document.getElementById("dropdownSearchType");
  const artistRegexp = /[?&](artist)=/;
  let queryType;
  if (artistRegexp.test(window.location.search) || !window.location.search) {
    dropdownButton.innerText = "Artist";
    queryType = `artist`;
  } else {
    dropdownButton.innerText = "Event";
    queryType = `event`;
  }

  dropdownItems.forEach((item) => {
    item.addEventListener("click", (e) => {
      queryType = e.target.getAttribute("value");
      dropdownButton.innerText = e.target.innerText;

      // Clear the other query parameter when search type changes
      const newUrl = new URL(window.location);
      if (queryType === "artist") {
        newUrl.searchParams.delete("event");
      } else {
        newUrl.searchParams.delete("artist");
      }
      history.replaceState(null, "", newUrl);
    });
  });

  let timeout = null;

  const urlParams = new URLSearchParams(window.location.search);
  const query = urlParams.get(queryType);
  if (query) {
    searchBar.value = query;
  }

  searchBar.addEventListener("input", (event) => {
    const query = event.target.value;
    const newUrl = new URL(window.location);
    newUrl.searchParams.set(queryType, query);

    // Use replaceState to update the URL without reloading the page
    history.replaceState(null, "", newUrl);

    if (timeout) {
      clearTimeout(timeout);
    }

    timeout = setTimeout(() => {
      window.location.href = newUrl;
    }, 700);
  });
});

document.addEventListener("DOMContentLoaded", (event) => {
  const dropdownItems = document.querySelectorAll(".dropdown-item");
  const dropdownButton = document.getElementById("dropdownSearchType");

  dropdownItems.forEach((item) => {
    item.addEventListener("click", (e) => {
      const selectedValue = e.target.getAttribute("value");
      dropdownButton.innerText = e.target.innerText;
      // You can store the selected value in a hidden input or a variable as needed
      // For example: document.getElementById('hiddenInput').value = selectedValue;
    });
  });
});
