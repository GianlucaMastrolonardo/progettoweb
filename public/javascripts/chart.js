document.addEventListener("DOMContentLoaded", async (event) => {
  const artistName = document.URL.substring(document.URL.lastIndexOf("/") + 1);
  const response = await fetch(
    `http://localhost:3000/artist/${artistName}?format=json`,
    {
      method: "GET",
      headers: { Accept: "application/json" },
    }
  );

  let artistInfo;
  try {
    [artistInfo] = await response.json();
  } catch (error) {
    console.error(error);
  }

  const data = [];
  artistInfo.Artist_Events.map((event) => {
    data.push({
      EventName: event.Event_Name,
      EventCity: event.Event_City,
      EventPartecipants: event.Event_Partecipants,
    });
  });

  new Chart(document.getElementById("acquisitions"), {
    type: "bar",
    data: {
      labels: data.map((row) => row.EventName + " | " + row.EventCity),
      datasets: [
        {
          label: "Partecipants",
          data: data.map((row) => row.EventPartecipants),
          borderWidth: 1.5,
          borderRadius: 10,
          backgroundColor: "#b911cf",
          borderColor: "#ababab",
        },
      ],
    },
  });
});
