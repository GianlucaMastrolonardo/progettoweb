"use strict";

function renderMessage(message, messageType) {
  let iconClass;
  if (messageType === "danger") {
    iconClass = "nf-cod-error";
  } else if (messageType === "success") {
    iconClass = "nf-cod-check";
  }
  return `
  <div class="container text-center" id="error-msg">
  <div class="alert alert-${messageType} ms-auto me-auto" id="login-error" role="alert">
  <i class ="nf ${iconClass}"></i>
         <strong>${message}</strong>
</div>
</div>`;
}

function printMessage(message, messageType) {
  //Remove previously alerts...
  if (document.getElementsByClassName("alert")) {
    document.querySelectorAll(".alert").forEach((el) => el.remove());
  }
  //Display new alert
  const top = document.getElementById("welcome-section");
  top.insertAdjacentHTML("afterend", renderMessage(message, messageType));
}

function findModal(modalNumber) {
  if (modalNumber < 0) {
    throw new Error("Invalid Modal Number");
  }
  const modal = modalArr.find((modalEl) => {
    return modalEl._element.id === `modalDeleteEvent-${modalNumber}`;
  });
  return modal;
}

const modals = document.querySelectorAll(".modal");
const modalArr = [];
modals.forEach((modal) => {
  modalArr.push(new bootstrap.Modal(document.getElementById(modal.id)));
});

const buttons = document.querySelectorAll(".delete-event");

buttons.forEach((button) => {
  button.addEventListener("click", async () => {
    try {
      const response = await fetch(
        `${window.location.href}/event/${button.value}`,
        { method: "DELETE" }
      );
      //Hide modal
      const modal = findModal(button.value);
      if (modal) {
        console.log(modal);
        modal.hide();
      }
      response.status <= 300
        ? printMessage("Event Deleted Successfully", "success")
        : printMessage("Something went wrong", "danger");

      setTimeout(() => {
        location.reload();
      }, 3000);
    } catch (err) {
      console.error(err);
    }
  });
});
