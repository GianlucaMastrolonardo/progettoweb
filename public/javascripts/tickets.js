"use strict";

const tooltipTriggerList = document.querySelectorAll(
  '[data-bs-toggle="tooltip"]'
);
const tooltipList = [...tooltipTriggerList].map(
  (tooltipTriggerEl) => new bootstrap.Tooltip(tooltipTriggerEl)
);

function renderMessage(message, messageType) {
  let iconClass;
  if (messageType === "danger") {
    iconClass = "nf-cod-error";
  } else if (messageType === "success") {
    iconClass = "nf-cod-check";
  } else if (messageType === "warning") {
    iconClass = "nf-cod-warning";
  }
  return `
    <div class="container text-center" id="error-msg">
    <div class="alert alert-${messageType} ms-auto me-auto" id="login-error" role="alert">
    <i class ="nf ${iconClass}"></i>
           <strong>${message}</strong>
  </div>
  </div>`;
}

function printMessage(message, messageType) {
  //Remove previously alerts...
  if (document.getElementsByClassName("alert")) {
    document.querySelectorAll(".alert").forEach((el) => el.remove());
  }
  //Display new alert
  const top = document.getElementById("welcome-section");
  top.insertAdjacentHTML("afterend", renderMessage(message, messageType));
}

const buttons = document.querySelectorAll(".buy-ticket");

buttons.forEach((button) => {
  button.addEventListener("click", async () => {
    try {
      const response = await fetch(
        `http://localhost:3000/ticket/buy/${button.value}`,
        { method: "PUT" }
      );
      console.log(response);
      if (response.status < 300) {
        printMessage("Ticket Buyed Successfully", "success");
      } else {
        const responseBody = await response.text();
        printMessage(responseBody, "danger");
        return;
      }
      setTimeout(() => {
        window.location.href = "http://localhost:3000/user/@me";
      }, 4000);
    } catch (err) {
      console.error(err);
    }
  });
});
