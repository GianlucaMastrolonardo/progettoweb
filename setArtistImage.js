"use strict";
const { setArtistImageFromSpotify } = require("./spotify");

async function setArtistImage(artist) {
  if (!artist.Artist_IMG && artist.Artist_SpotifyID) {
    //Set image with spotify API's
    await setArtistImageFromSpotify(artist);
  } else if (artist.Artist_IMG) {
    //Use image from DB
    artist.Artist_IMG = `http://localhost:3000/img/artist/${artist.Artist_ID}`;
  } else if (!artist.Artist_IMG) {
    artist.Artist_IMG = `http://localhost:3000/img/fallback`;
  }

  return artist;
}

async function setArtistsImage(artists) {
  for (let artist of artists) {
    await setArtistImage(artist);
  }
  return artists;
}

module.exports = { setArtistImage, setArtistsImage };
